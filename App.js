import React from "react";
import { Home } from "./src/screens/Home";
import {
  NavigationContainer,
  DefaultTheme,
  DarkTheme,
  useTheme,
} from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { Search } from "./src/screens/search";
import { VideoPlayer } from "./src/screens/VideoPlayer";
import { Explore } from "./src/screens/Explore";
import { MaterialIcons } from "@expo/vector-icons";

import { reducer } from "./src/reducers/reducer";
import { themeReducer } from "./src/reducers/themeReducer";
import { Provider, useSelector } from "react-redux";
import { createStore, combineReducers } from "redux";

const rootReducer = combineReducers({
  cardData: reducer,
  darkMode: themeReducer,
});

const store = createStore(rootReducer);

const Stack = createStackNavigator();
const Tabs = createBottomTabNavigator();

const customDarkTheme = {
  ...DarkTheme,
  colors: {
    ...DarkTheme.colors,
    headerColor: "black",
    textColor: "#FFF",
    iconColor: "#FFF",
    tabIconsColor: "#FFF",
  },
};

const customDefaultTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    headerColor: "#fff",
    textColor: "black",
    iconColor: "black",
    tabIconsColor: "red",
  },
};

const Root = () => {
  const { colors } = useTheme();
  return (
    <Tabs.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ color }) => {
          let iconName;

          if (route.name === "Home") {
            iconName = "home";
          } else if (route.name === "Explore") {
            iconName = "explore";
          } else if (route.name === "Subscribe") {
            iconName = "subscriptions";
          }
          return <MaterialIcons name={iconName} size={32} color={color} />;
        },
      })}
      tabBarOptions={{
        activeTintColor: colors.tabIconsColor,
        inactiveTintColor: "gray",
      }}
    >
      <Tabs.Screen name="Home" component={Home} />
      <Tabs.Screen name="Explore" component={Explore} />
    </Tabs.Navigator>
  );
};

export default () => {
  return (
    <Provider store={store}>
      <Navigation />
    </Provider>
  );
};

export function Navigation() {
  const theme = useSelector((state) => state.darkMode);
  return (
    <NavigationContainer theme={theme ? customDarkTheme : customDefaultTheme}>
      <Stack.Navigator headerMode="none">
        <Stack.Screen name="root" component={Root} />
        <Stack.Screen name="search" component={Search} />
        <Stack.Screen name="videoPlayer" component={VideoPlayer} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
