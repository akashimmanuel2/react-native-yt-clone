import React from "react";
import { Image, Text, View, Dimensions, TouchableOpacity } from "react-native";
import { MaterialIcons } from "@expo/vector-icons";
import { useNavigation, useTheme } from "@react-navigation/native";

export const Card = (props) => {
  const navigation = useNavigation();
  const { colors } = useTheme();
  return (
    <TouchableOpacity
      onPress={() =>
        navigation.navigate("videoPlayer", {
          id: props.data.id.videoId,
          title: props.data.snippet.title,
        })
      }
    >
      <View style={{ margin: 2, marginBottom: 10 }}>
        <Image
          source={{
            uri: `https://i.ytimg.com/vi/${props.data.id.videoId}/hqdefault.jpg`,
          }}
          style={{ width: "100%", height: 200 }}
        />
        <View style={{ flexDirection: "row", margin: 5 }}>
          <MaterialIcons name="account-circle" size={28} color="#212121" />
          <View style={{ marginLeft: 10 }}>
            <Text
              style={{
                fontSize: 20,
                width: Dimensions.get("screen").width - 50,
                color: colors.textColor,
              }}
              ellipsizeMode="tail"
              numberOfLines={2}
            >
              {props.data.snippet.title}
            </Text>
            <Text style={{ color: colors.textColor }}>
              {props.data.snippet.channelTitle}
            </Text>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
};
