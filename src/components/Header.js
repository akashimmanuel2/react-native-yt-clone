import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { AntDesign, Ionicons, MaterialIcons } from "@expo/vector-icons";
import { useNavigation, useTheme } from "@react-navigation/native";
import { useDispatch, useSelector } from "react-redux";

export const Header = () => {
  const { colors } = useTheme();
  const theme = useSelector((state) => state.darkMode);
  const dispatch = useDispatch();
  const navigation = useNavigation();

  const color = colors.iconColor;

  return (
    <View
      style={{
        height: 40,
        backgroundColor: colors.headerColor,
        flexDirection: "row",
        justifyContent: "space-between",
        elevation: 5,
      }}
    >
      <View style={styles.logoAndTitleView}>
        <AntDesign
          style={{ marginLeft: 5 }}
          name="youtube"
          size={28}
          color="red"
        />
        <Text
          style={{
            fontSize: 20,
            marginLeft: 10,
            fontWeight: "bold",
            color: colors.textColor,
          }}
        >
          YouTube
        </Text>
      </View>
      <View style={styles.headerRightSideIcons}>
        <Ionicons name="md-videocam" size={28} color={color} />
        <Ionicons
          name="md-search"
          size={28}
          color={color}
          onPress={() => navigation.navigate("search")}
        />
        <MaterialIcons
          name="account-circle"
          size={28}
          color={color}
          onPress={() => dispatch({ type: "CHANGE_THEME", payload: !theme })}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  logoAndTitleView: { flexDirection: "row", margin: 5 },
  logoText: {
    fontSize: 20,
    marginLeft: 10,
    fontWeight: "bold",
    color: "#212121",
  },
  headerRightSideIcons: {
    flexDirection: "row",
    margin: 5,
    justifyContent: "space-between",
    width: 110,
  },
});


