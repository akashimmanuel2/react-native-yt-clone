import React from "react";
import { Text, View, Image, Dimensions, TouchableOpacity } from "react-native";
import { useNavigation, useTheme } from "@react-navigation/native";

export const MiniCard = (props) => {
  const { colors } = useTheme();
  const navigation = useNavigation();
  return (
    <TouchableOpacity
      onPress={() =>
        navigation.navigate("videoPlayer", {
          id: props.data.id.videoId,
          title: props.data.snippet.title,
        })
      }
    >
      <View style={{ flex: 1, flexDirection: "row", margin: 5 }}>
        <Image
          source={{
            uri: `https://i.ytimg.com/vi/${props.data.id.videoId}/hqdefault.jpg`,
          }}
          style={{ width: "45%", height: 100 }}
        />
        <View style={{ marginLeft: 10 }}>
          <Text
            style={{
              fontSize: 15,
              width: Dimensions.get("screen").width / 2,
              color: colors.textColor,
            }}
            ellipsizeMode="tail"
            numberOfLines={3}
          >
            {props.data.snippet.title}
          </Text>
          <Text style={{ fontSize: 12, color: colors.textColor }}>
            {props.data.snippet.channelTitle}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};
