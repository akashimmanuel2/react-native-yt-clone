const initialState = [];

export const reducer = (state = initialState, action) => {
  if (action.type == "ADD_DATA") {
    return action.payload;
  }
  return state;
};
