import React from "react";
import { Text, View, FlatList, ScrollView } from "react-native";
import Constants from "expo-constants";
import { Header } from "../components/Header";

import { useSelector } from "react-redux";
import { Card } from "../components/Card";

const LittleCard = ({ title }) => {
  return (
    <View
      style={{
        backgroundColor: "red",
        width: 180,
        borderRadius: 4,
        height: 50,
        marginTop: 10,
      }}
    >
      <Text
        style={{
          textAlign: "center",
          color: "white",
          fontSize: 22,
          marginTop: 5,
        }}
      >
        {title}
      </Text>
    </View>
  );
};

export const Explore = () => {
  const CardData = useSelector((state) => state.cardData);

  return (
    <View style={{ flex: 1, marginTop: Constants.statusBarHeight }}>
      <ScrollView>
        <Header />
        <View
          style={{
            flexDirection: "row",
            flexWrap: "wrap",
            justifyContent: "space-around",
          }}
        >
          <LittleCard title="Gaming" />
          <LittleCard title="Trending" />
          <LittleCard title="Music" />
          <LittleCard title="News" />
          <LittleCard title="Movies" />
          <LittleCard title="Fashion" />
        </View>
        <Text style={{ fontSize: 22, borderBottomWidth: 1, margin: 10 }}>
          Trending Videos
        </Text>

        <FlatList
          data={CardData}
          keyExtractor={(e) => `${e.id.videoId}`}
          renderItem={({ item }) => <Card data={item} />}
        />
      </ScrollView>
    </View>
  );
};
