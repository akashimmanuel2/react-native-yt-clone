import React, { useEffect, useState } from "react";
import { View, FlatList } from "react-native";
import { Card } from "../components/Card";
import { Header } from "../components/Header";
import Constants from "expo-constants";

import { useSelector, useDispatch } from "react-redux";

export const Home = () => {
  const CardData = useSelector((state) => state.cardData);
  const [loading, setLoading] = useState(false);

  const dispatch = useDispatch();

  const fetchData = () => {
    fetch(`https://youtube.googleapis.com/youtube/v3/search?part=snippet&maxResults=100&q=recenttrending&type=video&key=<APIKEY>
      `)
      .then((res) => res.json())
      .then((e) => {
        dispatch({ type: "ADD_DATA", payload: e.items });
      })
      .catch((e) => console.log(e));
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <View style={{ flex: 1, marginTop: Constants.statusBarHeight }}>
      <Header />
      <FlatList
        data={CardData}
        keyExtractor={(e) => `${e.id.videoId}`}
        renderItem={({ item }) => <Card data={item} />}
        refreshing={loading}
        onRefresh={() => fetchData()}
      />
    </View>
  );
};
