import React from "react";
import { Text, View, FlatList, Dimensions } from "react-native";
import Constants from "expo-constants";
import { WebView } from "react-native-webview";
import { useSelector, useDispatch } from "react-redux";
import { MiniCard } from "../components/MiniCard";

export const VideoPlayer = (props) => {
  const MiniCardData = useSelector((state) => state);
  return (
    <View style={{ flex: 1, marginTop: Constants.statusBarHeight }}>
      <View style={{ width: "100%", height: 200 }}>
        <WebView
          javaScriptEnabled={true}
          domStorageEnabled={true}
          source={{
            uri: `https://www.youtube.com/embed/${props.route.params.id}`,
          }}
        />
      </View>
      <Text
        style={{
          fontSize: 20,
          width: Dimensions.get("screen").width - 50,
          margin: 9,
        }}
        numberOfLines={2}
        ellipsizeMode="tail"
      >
        {props.route.params.title}
      </Text>
      <View style={{ borderBottomWidth: 1 }} />
      <FlatList
        data={MiniCardData}
        keyExtractor={(e) => `${e.id.videoId}`}
        renderItem={({ item }) => <MiniCard data={item} />}
      />
    </View>
  );
};
