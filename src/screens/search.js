import React, { useState } from "react";
import { View, TextInput, FlatList, ActivityIndicator } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { MiniCard } from "../components/MiniCard";
import Constants from "expo-constants";

import { useSelector, useDispatch } from "react-redux";
import { useTheme } from "@react-navigation/native";

export const Search = (props) => {
  const { colors } = useTheme();

  const color = colors.iconColor;
  const [Value, setValue] = useState("");
  const [loading, setLoading] = useState(false);

  const dispatch = useDispatch();

  const MiniCardData = useSelector((state) => state.cardData);

  const fetchData = () => {
    setLoading(true);
    fetch(`https://youtube.googleapis.com/youtube/v3/search?part=snippet&maxResults=100&q=${Value}&type=video&key=<APIKEY
      `)
      .then((res) => res.json())
      .then((e) => {
        dispatch({ type: "ADD_DATA", payload: e.items });
        setLoading(false);
      })
      .catch((e) => console.log(e));
  };

  return (
    <View style={{ flex: 1, marginTop: Constants.statusBarHeight }}>
      <View
        style={{
          flexDirection: "row",
          padding: 5,
          justifyContent: "space-around",
          backgroundColor: colors.headerColor,
          elevation: 5,
        }}
      >
        <Ionicons
          name="md-arrow-back"
          size={32}
          onPress={() => props.navigation.goBack()}
          style={{ color: colors.iconColor }}
        />
        <TextInput
          style={{ width: "70%", backgroundColor: "#e6e6e6" }}
          value={Value}
          onChangeText={(e) => setValue(e)}
        />
        <Ionicons
          name="md-send"
          size={32}
          onPress={() => fetchData()}
          style={{ color: colors.iconColor }}
        />
      </View>

      {loading ? (
        <ActivityIndicator size="large" color="red" style={{ marginTop: 10 }} />
      ) : null}

      <FlatList
        data={MiniCardData}
        keyExtractor={(e) => `${e.id.videoId}`}
        renderItem={({ item }) => <MiniCard data={item} />}
      />
    </View>
  );
};
